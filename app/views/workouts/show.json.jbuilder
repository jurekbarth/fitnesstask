json.id @workout.id
json.name @workout.name
json.exercises @workout_exercises.each do |workout|
	json.exercise_name workout.exercise.name
end
