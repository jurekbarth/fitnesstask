json.array!(@trainings) do |training|
  json.extract! training, :id, :name, :notes, :starttime, :endtime, :workout_id
  json.url training_url(training, format: :json)
end
