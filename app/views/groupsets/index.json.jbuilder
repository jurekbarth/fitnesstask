json.array!(@groupsets) do |groupset|
  json.extract! groupset, :id, :training_id, :exercise_id
  json.url groupset_url(groupset, format: :json)
end
