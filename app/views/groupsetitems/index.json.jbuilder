json.array!(@groupsetitems) do |groupsetitem|
  json.extract! groupsetitem, :id, :repetition, :kgweight, :lbweight, :groupset_id
  json.url groupsetitem_url(groupsetitem, format: :json)
end
