class GroupsetitemsController < ApplicationController
  before_action :set_groupsetitem, only: [:show, :edit, :update, :destroy]

  # GET /groupsetitems
  # GET /groupsetitems.json
  def index
    @groupsetitems = Groupsetitem.all
  end

  # GET /groupsetitems/1
  # GET /groupsetitems/1.json
  def show
  end

  # GET /groupsetitems/new
  def new
    @groupsetitem = Groupsetitem.new
  end

  # GET /groupsetitems/1/edit
  def edit
  end

  # POST /groupsetitems
  # POST /groupsetitems.json
  def create
    @groupsetitem = Groupsetitem.new(groupsetitem_params)

    respond_to do |format|
      if @groupsetitem.save
        format.html { redirect_to @groupsetitem, notice: 'Groupsetitem was successfully created.' }
        format.json { render action: 'show', status: :created, location: @groupsetitem }
      else
        format.html { render action: 'new' }
        format.json { render json: @groupsetitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groupsetitems/1
  # PATCH/PUT /groupsetitems/1.json
  def update
    respond_to do |format|
      if @groupsetitem.update(groupsetitem_params)
        format.html { redirect_to @groupsetitem, notice: 'Groupsetitem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @groupsetitem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groupsetitems/1
  # DELETE /groupsetitems/1.json
  def destroy
    @groupsetitem.destroy
    respond_to do |format|
      format.html { redirect_to groupsetitems_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groupsetitem
      @groupsetitem = Groupsetitem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groupsetitem_params
      params.require(:groupsetitem).permit(:repetition, :kgweight, :lbweight, :groupset_id)
    end
end
