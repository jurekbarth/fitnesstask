class GroupsetsController < ApplicationController
  before_action :set_groupset, only: [:show, :edit, :update, :destroy]

  # GET /groupsets
  # GET /groupsets.json
  def index
    @groupsets = Groupset.all
  end

  # GET /groupsets/1
  # GET /groupsets/1.json
  def show
  end

  # GET /groupsets/new
  def new
    @groupset = Groupset.new
    @groupset.groupsetitems.build
  end

  # GET /groupsets/1/edit
  def edit
  end

  # POST /groupsets
  # POST /groupsets.json
  def create
    @groupset = Groupset.new(groupset_params)

    respond_to do |format|
      if @groupset.save
        format.html { redirect_to @groupset, notice: 'Groupset was successfully created.' }
        format.json { render action: 'show', status: :created, location: @groupset }
      else
        format.html { render action: 'new' }
        format.json { render json: @groupset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /groupsets/1
  # PATCH/PUT /groupsets/1.json
  def update
    respond_to do |format|
      if @groupset.update(groupset_params)
        format.html { redirect_to @groupset, notice: 'Groupset was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @groupset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groupsets/1
  # DELETE /groupsets/1.json
  def destroy
    @groupset.destroy
    respond_to do |format|
      format.html { redirect_to groupsets_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_groupset
      @groupset = Groupset.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def groupset_params
      params.require(:groupset).permit(:training_id, :exercise_id)
    end
end
