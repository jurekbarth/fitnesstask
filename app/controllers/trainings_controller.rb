class TrainingsController < ApplicationController
  before_action :set_training, only: [:show, :edit, :update, :destroy]

  # GET /trainings
  # GET /trainings.json
  def index
    @trainings = Training.all
  end

  # GET /trainings/1
  # GET /trainings/1.json
  def show
    @training_groupset = Groupset.where(:training_id => params[:id])
    @training_exercise = Exercise.where(:id => @training_groupset)
    @training_groupset_item = Groupsetitem.where(:groupset_id => @training_groupset)
  end

  # GET /trainings/new
  def new
    if params[:id].present?
      @training = Training.new(:workout_id => params[:id])
      @exercise_array = Workoutexercise.where(:workout_id => params[:id]).map(&:exercise_id)
      for i in 0..(@exercise_array.count-1)
        @training.groupsets.build(:exercise_id => @exercise_array[i])
      end
    else
      @training = Training.new      
    end
  end

  # GET /trainings/1/edit
  def edit
  end

  # POST /trainings
  # POST /trainings.json
  def create
    @training = Training.new(training_params)

    respond_to do |format|
      if @training.save

        @training.workout.exercises << @training.exercises
        @training.workout.save

        format.html { redirect_to @training, notice: 'Training was successfully created.' }
        format.json { render action: 'show', status: :created, location: @training }
      else
        format.html { render action: 'new' }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /trainings/1
  # PATCH/PUT /trainings/1.json
  def update
    respond_to do |format|
      if @training.update(training_params)
        format.html { redirect_to @training, notice: 'Training was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /trainings/1
  # DELETE /trainings/1.json
  def destroy
    @training.destroy
    respond_to do |format|
      format.html { redirect_to trainings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_training
      @training = Training.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def training_params
      params.require(:training).permit!
    end
end
