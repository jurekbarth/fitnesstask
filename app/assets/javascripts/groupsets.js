jQuery( document ).ready(function( $ ) {
  refresh_select2_exercise();
  $(".add_nested_fields").click(function() {
	  setTimeout((function() {
	  	refresh_select2_exercise();
	    //console.log('ready');
	  }), 10);
	});

	function refresh_select2_exercise() {
    var options = $('.autocomplete_exercise').last().data('autocomplete-source');
    //console.log(options);
		$(".autocomplete_exercise").select2({
	    multiple:false,
	    query:function(query) {
	        var data={results:[]}, 
	            results=data.results, 
	            t=query.term;

	        if (t!==""&&options.indexOf(t)<0) {
	            results.push({id:t, text:t});
	        }
	        
	        $(options)
	         .filter(function() { return this.indexOf(t)>=0; })
	         .each(function() { results.push({id:this, text:this}); });

	        query.callback(data);
	    },
	    initSelection:function (element, callback) {
	    	var data = {id: element.val(), text: element.val()};
        callback(data);
  		}
		});
  }

});