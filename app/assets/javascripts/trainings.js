jQuery( document ).ready(function( $ ) {
  var options = $('.autocomplete_workout').data('autocomplete-source');
	$(".autocomplete_workout").select2({
	    multiple:false,
	    query:function(query) {
	        var data={results:[]}, 
	            results=data.results, 
	            t=query.term;

	        if (t!==""&&options.indexOf(t)<0) {
	            results.push({id:t, text:t});
	        }
	        
	        $(options)
	         .filter(function() { return this.indexOf(t)>=0; })
	         .each(function() { results.push({id:this, text:this}); });

	        query.callback(data);
	    },
	    initSelection:function (element, callback) {
	    	var data = {id: element.val(), text: element.val()};
        callback(data);
  		}
	});

	$("#submit-form-button").click(function() {
		$('form').submit()
	});
});