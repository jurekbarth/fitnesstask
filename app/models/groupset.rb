class Groupset < ActiveRecord::Base
  belongs_to :training
  belongs_to :exercise
  has_many :groupsetitems

  accepts_nested_attributes_for :groupsetitems

  def find_groupsetitems(params)
    Groupsetitem.where(:groupset_id => params[:id])
  end

  def exercise_name
    exercise.try(:name)
  end
  
  def exercise_name=(name)
    self.exercise = Exercise.where(:name => name).first_or_create
  end

end
