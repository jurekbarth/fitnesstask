class Training < ActiveRecord::Base
  belongs_to :workout
  has_many :groupsets
  has_many :exercises, :through => :groupsets
  accepts_nested_attributes_for :groupsets

  # Helpermethod for displaying a hole training with sets and exercisese
  def find_groupset(id)
  	Groupset.where(:training_id => (id))
  end


  # For automcomplete and prevent simple duplication
  def workout_name
    workout.try(:name)
  end
  
  def workout_name=(name)
    self.workout = Workout.where(:name => name).first_or_create
  end

end
