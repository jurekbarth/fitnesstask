class Workout < ActiveRecord::Base
	has_many :trainings
	has_many :workoutexercises
  has_many :exercises, through: :workoutexercises
  attr_reader :exercise_tokens
  
  def exercise_tokens=(ids)
    self.exercise_ids = ids.split(",")
  end
  
end
