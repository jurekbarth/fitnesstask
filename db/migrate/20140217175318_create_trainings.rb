class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.string :name
      t.text :notes
      t.datetime :starttime
      t.datetime :endtime
      t.belongs_to :workout, index: true

      t.timestamps
    end
  end
end
