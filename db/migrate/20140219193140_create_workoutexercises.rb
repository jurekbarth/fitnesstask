class CreateWorkoutexercises < ActiveRecord::Migration
  def change
    create_table :workoutexercises do |t|
    	t.integer :ordernumber
    	t.belongs_to :workout
    	t.belongs_to :exercise

      t.timestamps
    end
  end
end
