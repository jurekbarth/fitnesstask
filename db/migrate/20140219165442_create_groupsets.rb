class CreateGroupsets < ActiveRecord::Migration
  def change
    create_table :groupsets do |t|
      t.belongs_to :training, index: true
      t.belongs_to :exercise, index: true

      t.timestamps
    end
  end
end
