class CreateGroupsetitems < ActiveRecord::Migration
  def change
    create_table :groupsetitems do |t|
    	t.integer :setnumber
      t.integer :repetition
      t.float :kgweight
      t.float :lbweight
      t.belongs_to :groupset, index: true

      t.timestamps
    end
  end
end
