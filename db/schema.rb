# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140219193140) do

  create_table "exercises", force: true do |t|
    t.string   "name"
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groupsetitems", force: true do |t|
    t.integer  "setnumber"
    t.integer  "repetition"
    t.float    "kgweight"
    t.float    "lbweight"
    t.integer  "groupset_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groupsetitems", ["groupset_id"], name: "index_groupsetitems_on_groupset_id"

  create_table "groupsets", force: true do |t|
    t.integer  "training_id"
    t.integer  "exercise_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groupsets", ["exercise_id"], name: "index_groupsets_on_exercise_id"
  add_index "groupsets", ["training_id"], name: "index_groupsets_on_training_id"

  create_table "trainings", force: true do |t|
    t.string   "name"
    t.text     "notes"
    t.datetime "starttime"
    t.datetime "endtime"
    t.integer  "workout_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "trainings", ["workout_id"], name: "index_trainings_on_workout_id"

  create_table "workoutexercises", force: true do |t|
    t.integer  "ordernumber"
    t.integer  "workout_id"
    t.integer  "exercise_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "workouts", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
