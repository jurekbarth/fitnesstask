require 'test_helper'

class GroupsetsControllerTest < ActionController::TestCase
  setup do
    @groupset = groupsets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:groupsets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create groupset" do
    assert_difference('Groupset.count') do
      post :create, groupset: { exercise_id: @groupset.exercise_id, training_id: @groupset.training_id }
    end

    assert_redirected_to groupset_path(assigns(:groupset))
  end

  test "should show groupset" do
    get :show, id: @groupset
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @groupset
    assert_response :success
  end

  test "should update groupset" do
    patch :update, id: @groupset, groupset: { exercise_id: @groupset.exercise_id, training_id: @groupset.training_id }
    assert_redirected_to groupset_path(assigns(:groupset))
  end

  test "should destroy groupset" do
    assert_difference('Groupset.count', -1) do
      delete :destroy, id: @groupset
    end

    assert_redirected_to groupsets_path
  end
end
