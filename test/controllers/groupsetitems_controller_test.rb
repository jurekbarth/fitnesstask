require 'test_helper'

class GroupsetitemsControllerTest < ActionController::TestCase
  setup do
    @groupsetitem = groupsetitems(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:groupsetitems)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create groupsetitem" do
    assert_difference('Groupsetitem.count') do
      post :create, groupsetitem: { groupset_id: @groupsetitem.groupset_id, kgweight: @groupsetitem.kgweight, lbweight: @groupsetitem.lbweight, repetition: @groupsetitem.repetition }
    end

    assert_redirected_to groupsetitem_path(assigns(:groupsetitem))
  end

  test "should show groupsetitem" do
    get :show, id: @groupsetitem
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @groupsetitem
    assert_response :success
  end

  test "should update groupsetitem" do
    patch :update, id: @groupsetitem, groupsetitem: { groupset_id: @groupsetitem.groupset_id, kgweight: @groupsetitem.kgweight, lbweight: @groupsetitem.lbweight, repetition: @groupsetitem.repetition }
    assert_redirected_to groupsetitem_path(assigns(:groupsetitem))
  end

  test "should destroy groupsetitem" do
    assert_difference('Groupsetitem.count', -1) do
      delete :destroy, id: @groupsetitem
    end

    assert_redirected_to groupsetitems_path
  end
end
